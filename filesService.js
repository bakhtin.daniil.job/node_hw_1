// requires...
const fs = require('fs')
const path = require('path');

// constants...
const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function checkExt (filename){
  return extensions.includes(path.extname(filename).slice(1));
}

function createFile (req, res, next) {
  const path = `./files/${req.body.filename}`;
  const filename = req.body.filename;
  const content = req.body.content;
  try{
    if (!req.body.filename) {
      res.status(400).send({ 'message': "Please specify 'filename' parameter" });
    }
    if (!req.body.filename.split('.').length) {
      res.status(400).send({ 'message': "Please specify 'filename' parameter" });
    }
    if (!req.body.content) {
      res.status(400).send({ 'message': "Please specify 'content' parameter" });
    }  
    if(!checkExt(filename)) 
  return res.status(400).send({ 'message': `File has invalid extension`});

  fs.writeFile(path, content, function (err) {
    if (err) throw err;
  });
  res.status(200).send({ "message": "File created successfully" });
} catch(err){
  res.status(400).send({
    "message": error.message
  })
}
}

function getFiles (req, res, next) {
  try{
  fs.readdir('./files', (err, file) => {
    if(err) throw err
    res.status(200).send({
      "message": "Success",
      "files": file});
  })
} catch(err){
  res.status(400).send({
    "message": error.message
  })
}
}

const getFile = (req, res, next) => {
  try{
    if (!fs.existsSync(`./files/${req.params.filename}`)) {
      res.status(400).send({ message: `No file with '${req.params.filename}' filename found` });
      return;
    }
  fs.readdir('./files', (err, files) => {
    if(err) throw err;
    files.forEach(item => {
      if(item === req.params.filename){
        fs.readFile(`./files/${item}`,'utf-8', (err, data) => {
          if(err) throw err;
          res.status(200).send({
            "message": "Success",
            "filename": item,
            "content": data,
            "extension": path.extname(item).split('.')[1],
            "uploadedDate": fs.statSync(`./files/${item}`).birthtime});
        })
      }
    })
  }) 
} catch(err){
  res.status(400).send({
    "message": error.message
  })
}
}

const editFile = (req, res, next) => {
  const path = `./files/${req.params.filename}`;
  const content = req.body.content;
  try{
  fs.writeFile(
    path,
    content,
    { 
      flag: "a"
    },
    (err) => {
      if(err)throw err 
    }
)
    res.status(200).send({
      "massage": 'file updated successfully'
    })
  } catch(err){
    res.status(400).send({
      "message": error.message
    })
  }
}


const deleteFile = (req, res, next) => {
    let path = req.params.filename;
    try{
    fs.unlink(`./files/${path}`, (err) => {
      if (err) {
        console.error(err)
        return 
      }
      res.status(200).send({
        "massage": `File ${path} deleted`
      })
    })
  } catch(err){
    res.status(400).send({
      "message": error.message
    })
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
